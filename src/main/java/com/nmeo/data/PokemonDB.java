package com.nmeo.data;

import com.nmeo.models.ModifyPokemonRequest;
import com.nmeo.models.Pokemon;
import com.nmeo.models.PokemonType;
import com.nmeo.models.Power;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// TODO perhaps I shouldn't share references to the calling functions, and make copies ?
public class PokemonDB {

    // Yes, this is not a real DB.
    private final HashMap<String, Pokemon> db = new HashMap<>();

    public @Nullable Pokemon findById(@Nonnull String id){
        return db.get(id);
    }

    public void addPokemon(@Nonnull Pokemon pokemon){
        synchronized (db) {
            db.put(pokemon.pokemonName, pokemon);
        }
    }

    public void updatePokemon(@Nonnull ModifyPokemonRequest request) throws NullPointerException {
        Pokemon pokemonToUpdate = findById(request.pokemonName);
        if(pokemonToUpdate == null) throw new NullPointerException("Pokemon doesn't exist !");

        // Step 2 apply new fields
        synchronized (db) {
            if(request.type != null) pokemonToUpdate.type = request.type;
            if(request.lifePoints != null) pokemonToUpdate.lifePoints = request.lifePoints;
            if(request.powers != null) {
                for (Power power : request.powers) {
                    if (pokemonToUpdate.powers.contains(power)) continue;
                    pokemonToUpdate.powers.add(power);
                }
            }
        }
    }

    /** List all pokemons containing the name passed as an argument */
    public List<Pokemon> findByName(@Nonnull String pokemonName){
        List<Pokemon> foundPokemons = new ArrayList<>();
        for(Pokemon pokemon: db.values()){
            if(pokemon.pokemonName.contains(pokemonName)) {
                foundPokemons.add(pokemon);
            }
        }

        return foundPokemons;
    }

    /** List all pokemons containing the type passed as an argument */
    public List<Pokemon> findByType(@Nonnull PokemonType type){
        // Else we're good
        List<Pokemon> foundPokemons = new ArrayList<>();
        for(Pokemon pokemon: db.values()){
            if(pokemon.type == type) {
                foundPokemons.add(pokemon);
            }
        }

        return foundPokemons;
    }
}
