package com.nmeo.models;

import java.util.List;

public class SearchResult {
    public List<Pokemon> result;

    public SearchResult(){}
    public SearchResult(List<Pokemon> result) {
        this.result = result;
    }
}
