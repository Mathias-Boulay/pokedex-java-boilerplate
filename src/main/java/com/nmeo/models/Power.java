package com.nmeo.models;

import java.util.Objects;

public class Power {
    public String powerName;
    public PokemonType damageType;
    public int damage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Power power = (Power) o;
        return damage == power.damage && Objects.equals(powerName, power.powerName) && damageType == power.damageType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(powerName, damageType, damage);
    }
}
