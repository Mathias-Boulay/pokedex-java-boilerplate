package com.nmeo.models;

import java.util.List;
import java.util.Optional;

public class ModifyPokemonRequest {
    public String pokemonName;
    public PokemonType type;
    public Integer lifePoints;
    public List<Power> powers;
}
