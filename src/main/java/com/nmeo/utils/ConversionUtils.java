package com.nmeo.utils;

import com.nmeo.App;
import com.nmeo.models.PokemonType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConversionUtils {
    private ConversionUtils(){}

    private static final Logger logger = LogManager.getLogger(App.class.getName());

    public static boolean isPokemonTypeEnum(String pokemonType){
        try {
            Enum.valueOf(PokemonType.class, pokemonType);
        } catch (IllegalArgumentException e) {
            logger.info("Illegal pokemon type enum");
            return false;
        }
        return true;
    }
}
