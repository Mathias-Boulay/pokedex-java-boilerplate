package com.nmeo;

import com.nmeo.data.PokemonDB;
import com.nmeo.models.*;
import io.javalin.Javalin;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import static com.nmeo.utils.ConversionUtils.isPokemonTypeEnum;

//TODO proper input validation, perhaps with a higher javalin version ?
public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());

    // Not good but I don't have the time to use a DB

    private static final PokemonDB DB_POKEMONS = new PokemonDB();


    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        Javalin.create()
        .get("/api/status", ctx -> {
            logger.debug("Status handler triggered", ctx);
            ctx.status(200);
        })
        .post("/api/create", ctx -> {
            Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
            if(pokemon == null || DB_POKEMONS.findById(pokemon.pokemonName) != null){
                ctx.status(400);
                return;
            }

            // Else we're good
            DB_POKEMONS.addPokemon(pokemon);
        })

        .get("/api/searchByName", ctx -> {
            String nameToSearch = ctx.queryParam("name");
            if(nameToSearch == null) {
                ctx.status(400);
                return;
            }

            ctx.json(new SearchResult(DB_POKEMONS.findByName(nameToSearch)));
            ctx.status(200);
        })

        .post("/api/modify", ctx -> {
            ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);

            try {
                DB_POKEMONS.updatePokemon(request);
            }catch (NullPointerException e){
                ctx.status(404);
                return;
            }

            // Else the update went as intended
            ctx.status(200);
        })

        .get("/api/searchByType", ctx -> {
            String typeToSearch = ctx.queryParam("type");
            if(typeToSearch == null || !isPokemonTypeEnum(typeToSearch)) {
                ctx.status(400);
                return;
            }

            PokemonType type = Enum.valueOf(PokemonType.class, typeToSearch);

            ctx.json(new SearchResult(DB_POKEMONS.findByType(type)));
            ctx.status(200);
        })
        .start(port);
    }
}